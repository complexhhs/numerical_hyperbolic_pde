# Describe Flux splitting form

## Gorverned Equation

du/dt + 1/2*d/dx(u^2) = 0

-> flux splitting form

du/dt + 1/2*d/dx(max(0,u)*u) + 1/2*d/dx(min(0,u)*u) = 0
