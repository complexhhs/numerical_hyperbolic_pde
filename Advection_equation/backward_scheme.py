import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('/home/hshwang/Simulation/numerical_hyperbolic_pde/etc')

from Initial_condition import Initial_condition

Initial_class = Initial_condition()
u_init = Initial_class.Boxcar_initial()
u_future = u_init.copy()

plt.plot(Initial_class.x_point,u_init,label='Initial state')

#print(u_init)

'''
Adavection equation:
(u^{k+1}-u^{k})/dt = -d(f^{k}_{j+1/2}-f^{k}_{j-1/2})/dx

where, f(u) = cu

Let C = +1 (right direction advection)

Backward advection equation scheme
f^{k}_{j+1/2} = f^{k}_{j}

CFL = 0.75
T_max = 2
B.C periodic condition
'''

dx = Initial_class.h
dt = 0.75*dx

Tmax = 2
t = np.arange(0,Tmax,dt)


for it,ts in enumerate(t):
    u_future[1:-1] = u_init[1:-1] - dt/dx*(u_init[1:-1]-u_init[:-2])
    u_future[0] = u_init[0] - dt/dx*(u_init[0]-u_init[-1])
    u_future[-1] = u_init[-1] - dt/dx*(u_init[-1]-u_init[-2])

    
    #if it == int(len(t)/2) or it == len(t)-1:
    if it % int(len(t)/4)==0 and it != 0 or it == len(t) -1:
        plt.plot(Initial_class.x_point,u_future,label=f'Time:{ts}')

    u_init = u_future

plt.grid()
plt.legend()
plt.savefig('./save_fig/Backward_scheme_result.png')
