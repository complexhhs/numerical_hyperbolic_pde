import numpy as np

class Initial_condition(object):
    def __init__(self,initial_point=-1,final_point=1,grid_point=1000):
        self.x_point = np.linspace(initial_point,final_point,grid_point)
        self.y_point = np.zeros_like(self.x_point)
        self.num_grid = grid_point
        self.start_point = initial_point
        self.final_point = final_point
        self.h = self.x_point[1]-self.x_point[0]
        self.k = 0.5*self.h
        self.n = grid_point

    def Boxcar_initial(self):
        idx = np.argwhere(np.abs(self.x_point)<0.25).ravel()
        self.y_point[idx] = 1
        return self.y_point

    def Sinusoid_initial(self):
       self.y_point[:] = np.sin(np.pi*self.x_point[:]+np.pi)
       return self.y_point

    def Gaussian_Boxcar_initial(self):
        self.y_point = np.exp(-((self.x_point+0.5)/2/0.1)**2) 
        box_idx = np.argwhere((self.x_point<=0.7) & (self.x_point>=0.3)).ravel()
        self.y_point[box_idx] = 1
        return self.y_point

def minmod(a,b):
    '''
    minmod function definition
    '''
    dummy_array = np.zeros_like(a)
    first_idx = np.argwhere(a*b<=0).ravel()
    a[first_idx] = 0
    b[first_idx] = 0

    second_idx = np.argwhere((np.abs(a) < np.abs(b)) & (a*b>0)).ravel()
    dummy_array[second_idx] = a[second_idx]

    third_idx = np.argwhere((np.abs(b) < np.abs(a)) & (a*b>0)).ravel()
    dummy_array[third_idx] = b[third_idx]
    return dummy_array

    #if np.abs(a) < np.abs(b) and a*b > 0:
    #    return a
    #elif np.abs(b) < np.abs(a) and a*b > 0:
    #    return b
    #else:
    #    return 0
