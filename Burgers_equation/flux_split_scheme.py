import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('/home/hshwang/Simulation/numerical_hyperbolic_pde/etc')

from Initial_condition import Initial_condition

Initial_class = Initial_condition()
u_init = Initial_class.Sinusoid_initial()
u_future = u_init.copy()

plt.plot(Initial_class.x_point,u_init,label='Initial state')

'''
Burger's equation:
(u^{k+1}-u^{k})/dt = -d(f^{k}_{j+1/2}-f^{k}_{j-1/2})/dx

where, f(u) = 1/2*u^2

Flux splitting scheme
u^K=1 = u^k 
        - lambda/2*(max(0,u^k_i)*u^k_i - max(0,u^k_i-1)*u^k_i-1)) <- leftward flux 
        - lambda/2*(min(0,u^k_i+1)*u^k_i+1 - min(0,u^k_i)*u^k_i)) <- rightward flux


CFL = 0.75
T_max = 0.5
B.C periodic condition
'''

dx = Initial_class.h
dt = 0.75*dx

Tmax = 0.55 
t = np.arange(0,Tmax,dt)

for it,ts in enumerate(t):
    u_future[1:-1] = u_init[1:-1] \
        -0.5*dt/dx*(np.where(u_init[1:-1]>=0,u_init[1:-1],0)*u_init[1:-1]-np.where(u_init[0:-2]>=0,u_init[0:-2],0)*u_init[0:-2])-0.5*dt/dx*(np.where(u_init[2:]<=0,u_init[2:],0)*u_init[2:]-np.where(u_init[1:-1]<=0,u_init[1:-1],0)*u_init[1:-1]) 

    u_future[0] = u_init[0]\
        -0.5*dt/dx*(max(0,u_init[0])*u_init[0]-max(0,u_init[-1])*u_init[-1])-0.5*dt/dx*(min(0,u_init[1])*u_init[1]-min(0,u_init[0])*u_init[0]) 

    u_future[-1] = u_future[0] # periodic boundary condition

    if it == int(len(t)/2) or it == len(t)-1:
        plt.plot(Initial_class.x_point,u_future,label=f'Time:{ts}')

    u_init = u_future

plt.legend()
plt.grid()
plt.savefig('./save_fig/flux_split_scheme_result.png')
