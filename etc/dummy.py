import numpy as np
import matplotlib.pyplot as plt
from Initial_condition import Initial_condition, minmod

#initial_class = Initial_condition()
#y = initial_class.Gaussian_Boxcar_initial()
#x = initial_class.x_point
#
#plt.plot(x,y)
#plt.show()  

a = np.random.uniform(-1,1,size=(5,))
b = np.random.uniform(-1,1,size=(5,))

print(f'a: {a}')
print(f'b: {b}')
output = minmod(a,b)
print(f'minmod: {output}')
