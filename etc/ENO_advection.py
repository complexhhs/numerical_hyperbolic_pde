'''
Goal: Simulate 3rd order accuracy ENO method for advection equation
Initial condition: Box-car initial
B.C: periodic boundary condition
'''
import os

if not os.path.isdir('./figure'):
    os.mkdir('./figure'):

if not os.path.isdir('./figure/ENO_advection'):
    os.mkdir('./figure/ENO_advection')

import numpy as np
import matplotlib.pyplot as plt

from initial_condition import Initial_condition

cond = Initial_condition()
u_present = cond.Boxcod_initial()
h = cond.h
k = cond.k
n = cond.n

u_new = np.zeros_like(u_present)
pnu_flux = np.zeros_like(u_present)
mnu_flux = np.zeros_like(u_present)
fin_flux = np.zeros_liek(u_present)

cond_left = np.zeros(size=(n,5))
cond_right = np.zeros(size=(n,5))

dh2 = np.zeros(n)
dh3 = np.zeros(n)

ddh1 = np.zeros(n)
ddh2 = np.zeros(n)
ddh3 = np.zeros(n)

Tmax = 2
t_range = np.arange(0,Tmax,k)

for it in t_range:
    for ix in range(n):
        pnu_flx[ix] = (u_present[ix]+max(u_present)*u_present[ix])/2
        
        # Application part: Newtown formulation 3rd oder accuracy polynomial
        for isten in range(5):
            k0 = isten-3
            k1 = 3-isten
            ik = ix+k0

            if ix >= 0 and ik <= n: # periodic boundary condition
                cond_left[ix,isten] = pnu_flx[ix]
            elif ik == 0:
                cond_left[ix,isten] = pnu_flx[ix-1]
            elif ik == -1:
                cond_left[ix,isten] = pnu_flx[ix-2]
            elif ik == n+1:
                cond_left[ix,isten] = pnu_flx[1]
            else:
                cond_left[ix,isten] = pnu_flx[2]

            
